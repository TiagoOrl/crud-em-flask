# coding:utf-8

from flask import Flask, render_template, flash, redirect, url_for

from loja_livros import app
from loja_livros.forms import RegistrarLivroForm
from loja_livros.models import Livro
from loja_livros import db


@app.route("/")
@app.route("/home")
def home_page():
    return render_template('home.html', pag_title = "Home")

@app.route("/registrar", methods=['GET', 'POST'])
def registrar_livro():

    livro_form = RegistrarLivroForm()

    if livro_form.validate_on_submit():   # se o objeto do form for validado com sucesso...
        livro = Livro(titulo = livro_form.titulo.data, autor = livro_form.autor.data, ano = livro_form.ano.data)  # passando os dados do form para o modelo e salvando no bando de dados
        db.session.add(livro)
        db.session.commit()

        flash('Livro adicionado com sucesso', 'success')
        return redirect(url_for('home_page'))

    return render_template('registrar.html', pag_title = "Adicionar Livro", form = livro_form, legenda = 'Inserir novo livro')


@app.route("/exibir")
def exibir_items():

    query_livros = Livro.query.all()
    return render_template('exibir.html', livros = query_livros, pag_title = "Listar Livros")


@app.route("/remover/<int:livro_id>")   # argumento necessário para passar o dado do livro para a rota
def remover_livro(livro_id):
    query_livro = Livro.query.get_or_404(livro_id)

    db.session.delete(query_livro)
    db.session.commit()

    flash('Livro removido com sucesso', 'success')
    return redirect(url_for('home_page'))



@app.route("/atualizar/<int:livro_id>", methods =['GET', 'POST'])
def atualizar_livro(livro_id):
    query_livro  = Livro.query.get_or_404(livro_id)

    form = RegistrarLivroForm()

    if form.validate_on_submit():
        query_livro.titulo = form.titulo.data
        query_livro.autor = form.autor.data
        query_livro.ano = form.ano.data
        db.session.commit()
        flash("Livro atualizado com sucesso!", "success")
        return redirect(url_for('exibir_items'))

    form.titulo.data = query_livro.titulo
    form.autor.data = query_livro.autor
    form.ano.data = query_livro.ano

    return render_template('registrar.html', pag_title = "Atualizar Livro", form = form, legenda = 'Atualizar Dados do livro')
