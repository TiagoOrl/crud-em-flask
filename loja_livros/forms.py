

from flask_wtf import Form
from wtforms import StringField, SubmitField, TextField
from wtforms.validators import DataRequired, Length


class RegistrarLivroForm(Form):
    titulo = StringField('Titulo', validators = [DataRequired()])

    autor = StringField('Autor', validators = [DataRequired()])

    ano = StringField('Ano', validators = [DataRequired(), Length(min = 3, max = 4)])

    add_livro_btn = SubmitField('Adicionar')