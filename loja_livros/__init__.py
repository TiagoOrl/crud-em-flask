# coding:utf-8


from flask import Flask
from flask_sqlalchemy import SQLAlchemy


app = Flask(__name__)

app.config['SECRET_KEY'] = '7505edf95abfde398198rrjk423ru239r832jf2398fjoewf2989f23de4c9b5dc46b10'
app.config['SQLALCHEMY_DATABASE_URI'] = 'sqlite:///site.db'     #especificando a localização do banco para o SQLAlchemy

db = SQLAlchemy(app)    #instanciando o banco de dados

from loja_livros.models import Livro
from loja_livros import routes
