# coding:utf-8

from loja_livros import db

class Livro(db.Model):      # uma tabela e suas colunas através da Classe Model do SQLAlchemy

    id = db.Column(db.Integer, primary_key = True)
    titulo = db.Column(db.String, unique = True, nullable = False)
    autor = db.Column(db.String, unique = False, nullable = False)
    ano = db.Column(db.Integer, unique = False, nullable = False)

    def __repr__(self):     # como o objeto sera mostrado:
        return "Livro(\'{}\', \'{}\', \'{}\')".format(self.titulo, self.autor, self.ano)



#   executar comando no python3: db.create_all() para criar as tabelas
